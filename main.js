var BASE_DN = 'dc=example, dc=com';
var BDN_LEN = BASE_DN.length + 2;
var BDN_END = ', ' + BASE_DN;

var ldap = require('ldapjs');
var mysql = require('mysql');
var kdf2 = require('pbkdf2-password');

var server = ldap.createServer();
var pool = mysql.createPool({
  host: 'localhost',
  user: 'ldap_manager',
  password: 'ldappassword',
  database: 'ldap'
});
var hasher = kdf2({digest: 'sha256', iterations: 1000, keyLength: 64});

var query_salt = 'SELECT salt FROM users WHERE cn = ?';
var query_login = 'SELECT cn FROM users WHERE cn = ? AND pw = FROM_BASE64(?)';
var query_info = 'SELECT cn, mail, displayname, memberof FROM users WHERE cn = ? AND permission = 0';
var query_log = 'INSERT INTO log (cn, act) VALUES (?, ?)';

var authz = new Set();
authz.add('ldap');

function authorize(req, res, next)
{
  var dn = req.connection.ldap.bindDN.toString();
  dn = dn.substring(3, dn.length - BDN_LEN);
  if(!authz.has(dn))
    return next(new ldap.InsufficientAccessRightsError());
  return next();
}

function findCN(filter)
{
  if(filter instanceof ldap.EqualityFilter)
  {
    if(filter.attribute === 'cn')
    {
      return filter.raw.toString();
    }
    return false;
  }
  if((filter instanceof ldap.AndFilter) || (filter instanceof ldap.OrFilter))
  {
    for(var i = 0; i < filter.filters.length; i++)
    {
      var cn = findCN(filter.filters[i]);
      if(cn !== false)
      {
        return cn;
      }
    }
  }
  return false;
}

server.bind('', function(req, res, next)
{
  var dn = req.dn.toString();
  if(!dn.startsWith('cn=') || !dn.endsWith(BDN_END))
  {
    return next(new ldap.InvalidCredentialsError());
  }
  dn = dn.substring(3, dn.length - BDN_LEN);
  pool.getConnection(function (err, connection)
  {
    if(err) return next(new ldap.BusyError());
    connection.query(mysql.format(query_salt, dn), function(err, result)
    {
      if(err)
      {
        connection.release();
        return next(new ldap.BusyError());
      }
      if(result.length !== 1)
      {
        connection.query(mysql.format(query_log, [dn, 2]), function(err, result)
        {
          connection.release();
          if(err) return next(new ldap.BusyError());
          return next(new ldap.InvalidCredentialsError());
        });
        return;
      }
      hasher({password: req.credentials, salt: result[0].salt.toString('base64')}, function(err, pass, salt, hash)
      {
        if(err)
        {
          connection.release();
          return next(new ldap.BusyError());
        }
        connection.query(mysql.format(query_login, [dn, hash]), function(err, result)
        {
          if(err)
          {
            connection.release();
            return next(new ldap.BusyError());
          }
          if(result.length !== 1)
          {
            connection.query(mysql.format(query_log, [dn, 2]), function(err, result)
            {
              connection.release();
              if(err) return next(new ldap.BusyError());
              return next(new ldap.InvalidCredentialsError());
            });
            return;
          }
          connection.query(mysql.format(query_log, [dn, 1]), function(err, result)
          {
            connection.release();
            if(err) return next(new ldap.BusyError());
            res.end();
            return next();
          });
        });
      });
    });
  });
});

server.search(BASE_DN, authorize, function(req, res, next)
{
  var cn = findCN(req.filter);
  if(cn === false)
  {
    res.end();
  }
  pool.getConnection(function (err, connection)
  {
    if(err) return next(new ldap.BusyError());
    connection.query(mysql.format(query_info, cn), function(err, result)
    {
      connection.release();
      if(err) return next(new ldap.BusyError());
      if(result.length === 1)
      {
        var obj = {
          dn: 'cn=' + cn + BDN_END,
          attributes: {
            objectclass: ['inetOrgPerson'],
            cn: cn,
            givenName: cn,
            mail: result[0].mail,
            displayName: result[0].displayname,
            memberof: result[0].memberof
          }
        };
        if(req.filter.matches(obj.attributes))
        {
          res.send(obj);
        }
      }
      res.end();
    });
  });
});

server.listen(389, function()
{
  console.log('LDAP server up at: %s', server.url);
});
