<?php
session_start();
if(!isset($_SESSION['cn']))
{
    header("Location: login.html");
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Home</title>
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        <hgroup>
            <h1>Welcome!</h1>
            <h2>Auth only LDAP</h2>
        </hgroup>
        <table>
            <tr>
                <td>ID</td>
                <td><input readonly="readonly" type="text" value="<?=htmlspecialchars($_SESSION['cn'])?>" /></td>
            </tr>
            <tr>
                <td>e-mail</td>
                <td><input readonly="readonly" type="text" value="<?=htmlspecialchars($_SESSION['mail'])?>" /></td>
            </tr>
            <tr>
                <td>Name</td>
                <td><input readonly="readonly" type="text" value="<?=htmlspecialchars($_SESSION['displayname'])?>" /></td>
            </tr>
            <tr>
                <td colspan="2"><input type="button" value="Logout" onclick="location.href = 'logout.php'" /></td>
            </tr>
        </table>
        <a href="update_pw.html">Update Password</a>
        <a href="update_info.php">Update Info</a>
<?php
if($_SESSION['permission'] === 1)
{
    ?>
        <a href="admin/">Admin</a>
<?php
}
?>
    </body>
</html>