window.addEventListener('load', function ()
{
    var form = document.forms.ldap;
    var pwcheck = false;
    function valid_pw(pw)
    {
        return pw.length >= 8;
    }
    function validate_pw()
    {
        if(valid_pw(form.pw.value))
        {
            form.classList.remove('invalid_pw');
        }
        else
        {
            form.classList.add('invalid_pw');
        }
        if(pwcheck)
        {
            check_pw_match();
        }
    }
    function check_pw_match(ev)
    {
        if(form.pw.value != '') pwcheck = true;
        if(form.pw.value === form.check.value)
        {
            form.classList.remove('pw_not_match');
        }
        else
        {
            form.classList.add('pw_not_match');
        }
    }
    form.pw.addEventListener('keydown', function (ev)
    {
        setTimeout(validate_pw, 100);
    });
    form.pw.addEventListener('focusout', validate_pw);
    form.check.addEventListener('keydown', function ()
    {
        setTimeout(check_pw_match, 100);
    });
    form.check.addEventListener('focusout', check_pw_match);
    document.querySelector('input[type="button"]').addEventListener('click', function ()
    {
        validate_pw();
        if(form.classList.contains('invalid_pw'))
        {
            alert('Invalid password');
            return;
        }
        check_pw_match();
        if(form.classList.contains('pw_not_match'))
        {
            alert('Check password');
            return;
        }
        form.pw.readonly = true;
        form.check.disabled = true;
        form.submit();
    });
});
