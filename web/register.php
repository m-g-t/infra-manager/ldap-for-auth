<?php
if($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['id'], $_POST['pw'], $_POST['mail'], $_POST['name']))
{
    header('Location: access.html');
    exit;
}
include '.htdbconfig.php';
$stmt = $conn->prepare('INSERT IGNORE INTO users (cn, mail, displayname, salt, pw) VALUES (?, ?, ?, UNHEX(?), UNHEX(?))');
if(!$stmt)
{
    header('Location: error.html');
    $conn->close();
    exit;
}
$salt = random_bytes(64);
$pass = bin2hex(hash_pbkdf2('sha256', $_POST['pw'], $salt, 1000, 64, true));
$salt = bin2hex($salt);
$stmt->bind_param('sssss', $_POST['id'], $_POST['mail'], $_POST['name'], $salt, $pass);
$stmt->execute();
if($stmt->affected_rows === 1)
{
    $stmt->close();
    $stmt = $conn->prepare('INSERT INTO log (cn, act) VALUES (?, 5)');
    if(!$stmt)
    {
        header('Location: error.html');
        $conn->close();
        exit;
    }
    $stmt->bind_param('s', $_POST['id']);
    $stmt->execute();
    header('Location: login.html');
}
else
{
    header('Location: register.html');
}
$stmt->close();
$conn->close();
?>