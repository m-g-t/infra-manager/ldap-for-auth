<?php
session_start();
if(isset($_SESSION['cn']))
{
    header('Location: .');
    exit;
}
if($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['id'], $_POST['pw']))
{
    header('Location: access.html');
    exit;
}
include '.htdbconfig.php';
$stmt = $conn->prepare('SELECT HEX(salt) s FROM users WHERE cn = ?');
if(!$stmt)
{
    header('Location: error.html');
    $conn->close();
    exit;
}
$stmt->bind_param('s', $_POST['id']);
$stmt->execute();
$result = $stmt->get_result();
if(!$result)
{
    header('Location: error.html');
    $stmt->close();
    $conn->close();
    exit;
}
if($result->num_rows > 0)
{
    $salt = hex2bin($result->fetch_assoc()['s']);
}
else
{
    $result->close();
    $stmt->close();
    $stmt = $conn->prepare('INSERT INTO log (cn, act) VALUES (?, 4)');
    if(!$stmt)
    {
        header('Location: error.html');
        $conn->close();
        exit;
    }
    $stmt->bind_param('s', $_POST['id']);
    $stmt->execute();
    $stmt->close();
    $conn->close();
    header('Location: password.html');
    exit;
}
$result->close();
$stmt->close();
$stmt = $conn->prepare('SELECT cn, mail, displayname, memberof, permission FROM users WHERE cn = ? AND pw = UNHEX(?)');
if(!$stmt)
{
    header('Location: error.html');
    $conn->close();
    exit;
}
$pass = bin2hex(hash_pbkdf2('sha256', $_POST['pw'], $salt, 1000, 64, true));
$stmt->bind_param('ss', $_POST['id'], $pass);
$stmt->execute();
$result = $stmt->get_result();
if(!$result)
{
    header('Location: error.html');
    $stmt->close();
    $conn->close();
    exit;
}
if($result->num_rows > 0)
{
    $row = $result->fetch_assoc();
    $_SESSION['cn'] = $row['cn'];
    $_SESSION['mail'] = $row['mail'];
    $_SESSION['displayname'] = $row['displayname'];
    $_SESSION['memberof'] = $row['memberof'];
    $_SESSION['permission'] = $row['permission'];
}
else
{
    $result->close();
    $stmt->close();
    $stmt = $conn->prepare('INSERT INTO log (cn, act) VALUES (?, 4)');
    if(!$stmt)
    {
        header('Location: error.html');
        $conn->close();
        exit;
    }
    $stmt->bind_param('s', $_POST['id']);
    $stmt->execute();
    $stmt->close();
    $conn->close();
    header('Location: password.html');
    exit;
}
$result->close();
$stmt->close();
$stmt = $conn->prepare('INSERT INTO log (cn, act) VALUES (?, 3)');
if(!$stmt)
{
    header('Location: error.html');
    $conn->close();
    exit;
}
$stmt->bind_param('s', $_POST['id']);
$stmt->execute();
$stmt->close();
$conn->close();
header('Location: .');
?>