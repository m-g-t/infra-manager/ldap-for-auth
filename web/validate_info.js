window.addEventListener('load', function ()
{
    var form = document.forms.ldap;
    function valid_mail(mail)
    {
        return true;//mail.endsWith('@example.com');
    }
    var re_mail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    function validate_mail()
    {
        if(re_mail.test(form.mail.value) && valid_mail(form.mail.value))
        {
            form.classList.remove('invalid_mail');
        }
        else
        {
            form.classList.add('invalid_mail');
        }
    }
    form.mail.addEventListener('keydown', function ()
    {
        setTimeout(validate_mail, 100);
    });
    form.mail.addEventListener('focusout', validate_mail);
    document.querySelector('input[type="button"]').addEventListener('click', function ()
    {
        validate_mail();
        if(form.classList.contains('invalid_mail'))
        {
            alert('Invalid e-mail');
            return;
        }
        form.submit();
    });
});
