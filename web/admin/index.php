<?php
session_start();
if(!isset($_SESSION['cn']))
{
    header('Location: ..');
}
if($_SESSION['permission'] !== 1)
{
    header('Location: ../access.html');
}
include '../.htdbconfig.php';
$result = $conn->query('SELECT COUNT(*) c FROM users');
if(!$result)
{
    header('Location: error.html');
    $conn->close();
    exit;
}
if($result->num_rows > 0)
{
    $count = $result->fetch_assoc()['c'];
}
$result->close();
$conn->close();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Admin</title>
        <link rel="stylesheet" href="../style.css" />
    </head>
    <body>
        <hgroup>
            <h1>Admin page</h1>
            <h2>Auth only LDAP</h2>
        </hgroup>
        <table>
            <tr>
                <td>Users</td>
                <td><input disabled="disabled" value="<?=$count?>" /></td>
            </tr>
            <tr>
                <td colspan="2"><input type="button" value="Manage Users" onclick="location.href = 'users.php'" /></td>
            </tr>
            <tr>
                <td colspan="2"><input type="button" value="Show Audit Log" onclick="location.href = 'log.php'" /></td>
            </tr>
        </table>
        <a href="..">Home</a>
    </body>
</html>