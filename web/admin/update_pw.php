<?php
session_start();
if(!isset($_SESSION['cn']))
{
    header('Location: ..');
    exit;
}
if($_SESSION['permission'] !== 1)
{
    header('Location: ../access.html');
    exit;
}
if(!isset($_GET['seq']))
{
    header('Location: ../access.html');
    exit;
}
include '../.htdbconfig.php';
$stmt = $conn->prepare('SELECT cn FROM users WHERE seq = ?');
if(!$stmt)
{
    header('Location: error.html');
    $conn->close();
    exit;
}
$stmt->bind_param('s', $_GET['seq']);
$stmt->execute();
$result = $stmt->get_result();
if(!$result)
{
    header('Location: error.html');
    $stmt->close();
    $conn->close();
    exit;
}
if($result->num_rows > 0)
{
    $row = $result->fetch_assoc();
}
else
{
    header('Location: ../access.html');
    $result->close();
    $stmt->close();
    $conn->close();
    exit;
}
$result->close();
$stmt->close();
$conn->close();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Update</title>
        <link rel="stylesheet" href="../style.css" />
        <link rel="stylesheet" href="../validate.css" />
        <script src="../validate_pw.js"></script>
    </head>
    <body>
        <hgroup>
            <h1>Update</h1>
            <h2>Auth only LDAP</h2>
        </hgroup>
        <form name="ldap" method="POST" action="update.php">
            <table>
                <tr>
                    <td>LDAP ID</td>
                    <td><input readonly="readonly" type="text" name="cn" value="<?=htmlspecialchars($row['cn'])?>" /></td>
                </tr>
                <tr>
                    <td>New PW</td>
                    <td><input type="password" name="pw" /></td>
                </tr>
                <tr>
                    <td>retype PWk</td>
                    <td><input type="password" name="check" /></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="button" value="Update" /></td>
                </tr>
            </table>
        </form>
        <a href="update_pw.php">Update Password</a>
        <a href=".">cancel</a>
    </body>
</html>