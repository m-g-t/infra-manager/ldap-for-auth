<?php
session_start();
if(!isset($_SESSION['cn']))
{
    header('..');
    exit;
}
if($_SESSION['permission'] !== 1)
{
    header('Location: ../access.html');
    exit;
}
if($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['mail'], $_POST['name'], $_POST['cn']))
{
    header('Location: ../access.html');
    exit;
}
include '../.htdbconfig.php';
$stmt = $conn->prepare('UPDATE users SET mail = ?, displayname = ? WHERE cn = ?');
if(!$stmt)
{
    header('Location: error.html');
    $conn->close();
    exit;
}
$stmt->bind_param('sss', $_POST['mail'], $_POST['name'], $_POST['cn']);
$stmt->execute();
if($stmt->affected_rows === 1)
{
    $stmt->close();
    $stmt = $conn->prepare('INSERT INTO log (cn, act) VALUES (?, 8)');
    if(!$stmt)
    {
        header('Location: error.html');
        $conn->close();
        exit;
    }
    $stmt->bind_param('s', $_POST['cn']);
    $stmt->execute();
    header('Location: .');
}
else
{
    header('Location: users.php');
}
$stmt->close();
$conn->close();
?>