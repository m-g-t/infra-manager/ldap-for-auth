<?php
session_start();
if(!isset($_SESSION['cn']))
{
    header('Location: ..');
}
if($_SESSION['permission'] !== 1)
{
    header('Location: ../access.html');
}
$page_size = 10;
$page_index = 0;
if(isset($_GET['s']))
{
    $tmp = intval($_GET['s']);
    if($tmp) $page_size = $tmp;
}
if(isset($_GET['i']))
{
    $page_index = intval($_GET['i']) - 1;
}
$get_i = $page_index + 1;
$page_index *= $page_size;
include '../.htdbconfig.php';
if(!($result = $conn->query("SELECT seq, cn, mail, displayname FROM users ORDER BY cn ASC LIMIT $page_index, $page_size")))
{
    header('Location: ../error.html');
    $conn->close();
    exit;
}
if(!($row = $result->fetch_assoc()))
{
    header('Location: ../error.html');
    $result->close();
    $conn->close();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Manage Users</title>
        <link rel="stylesheet" href="../style.css" />
    </head>
    <body>
        <table>
            <tr>
                <th>#</th>
                <th>LDAP ID</th>
                <th>e-mail</th>
                <th>Name</th>
            </tr><?php
do
{
    echo "
            <tr>
                <td><a href=\"user.php?seq=$row[seq]\">$row[seq]</a></td>
                <td>" . htmlspecialchars($row['cn']) . '</td>
                <td>' . htmlspecialchars($row['mail']) . '</td>
                <td>' . htmlspecialchars($row['displayname']) . '</td>
            </tr>';
}
while($row = $result->fetch_assoc());
$result->close();
?>

        </table><?php
if(!($result = $conn->query("SELECT COUNT(*) FROM users")))
{
    echo '
        Fail to load the quantity';
}
else
{
    echo '
        <nav>';
    $count = $result->fetch_array()[0];
    $result->close();
    $page_count = floor($count / $page_size);
    $page_start = floor($page_index / 10 / $page_size) * 10;
    $tmp = $page_size != 10 ? "s=$page_size&i=" : 'i=';
    if($page_start)
    {
        echo "
            <a href=\"?$tmp$page_start\">Prev</a>";
    }
    for($i = 0; $i < 10; )
    {
        $i++;
        $temp = $page_start + $i;
        if($temp > floor(($count - 1) / $page_size) + 1) break;
        echo $get_i == $temp ? "
            <a>$temp</a>" : "
            <a href=\"?$tmp$temp\">$temp</a>";
    }
    if(($page_start + $page_size) * 10 < $count)
    {
        $temp = $page_start + 11;
        echo "
            <a href=\"?$tmp$temp\">Next</a>";
    }
    echo '
        </nav>';
}
$conn->close();
?>

        <a href="..">Home</a>
    </body>
</html>