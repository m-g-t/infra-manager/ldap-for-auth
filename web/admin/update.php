<?php
session_start();
if(!isset($_SESSION['cn']))
{
    header('Location: ..');
    exit;
}
if($_SESSION['permission'] !== 1)
{
    header('Location: ../access.html');
}
if($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['pw'], $_POST['cn']))
{
    header('Location: ../access.html');
    exit;
}
include '../.htdbconfig.php';
$stmt = $conn->prepare('SELECT HEX(salt) s FROM users WHERE cn = ?');
if(!$stmt)
{
    header('Location: error.html');
    $conn->close();
    exit;
}
$stmt->bind_param('s', $_POST['cn']);
$stmt->execute();
$result = $stmt->get_result();
if(!$result)
{
    header('Location: error.html');
    $stmt->close();
    $conn->close();
    exit;
}
if($result->num_rows > 0)
{
    $salt = hex2bin($result->fetch_assoc()['s']);
}
else
{
    $result->close();
    $stmt->close();
    $conn->close();
    header('Location: error.html');
    exit;
}
$result->close();
$stmt->close();
$stmt = $conn->prepare('UPDATE users SET pw = UNHEX(?) WHERE cn = ?');
if(!$stmt)
{
    header('Location: error.html');
    $conn->close();
    exit;
}
$pass = bin2hex(hash_pbkdf2('sha256', $_POST['pw'], $salt, 1000, 64, true));
$stmt->bind_param('ss', $pass, $_POST['cn']);
$stmt->execute();
if($stmt->affected_rows === 1)
{
    $stmt->close();
    $stmt = $conn->prepare('INSERT INTO log (cn, act) VALUES (?, 9)');
    if(!$stmt)
    {
        header('Location: error.html');
        $conn->close();
        exit;
    }
    $stmt->bind_param('s', $_POST['cn']);
    $stmt->execute();
    header('Location: .');
}
else
{
    header('Location: update_pw.html');
}
$stmt->close();
$conn->close();
?>