<?php
session_start();
if(!isset($_SESSION['cn']))
{
    header('Location: login.php');
    exit;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Update</title>
        <link rel="stylesheet" href="style.css" />
        <link rel="stylesheet" href="validate.css" />
        <script src="validate_info.js"></script>
    </head>
    <body>
        <hgroup>
            <h1>Update</h1>
            <h2>Auth only LDAP</h2>
        </hgroup>
        <form name="ldap" method="POST" action="update.php">
            <table>
                <tr>
                    <td>Current PW</td>
                    <td><input type="password" name="old_pw" /></td>
                </tr>
                <tr>
                    <td>e-mail</td>
                    <td><input type="text" name="mail" value="<?=htmlspecialchars($_SESSION['mail'])?>" /></td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td><input type="text" name="name" value="<?=htmlspecialchars($_SESSION['displayname'])?>" /></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="button" value="Update" /></td>
                </tr>
            </table>
        </form>
        <a href=".">cancel</a>
    </body>
</html>