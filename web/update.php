<?php
session_start();
if(!isset($_SESSION['cn']))
{
    header('Location: login.php');
    exit;
}
if($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['old_pw'], $_POST['mail'], $_POST['name']))
{
    header('Location: access.html');
    exit;
}
include '.htdbconfig.php';
$stmt = $conn->prepare('SELECT HEX(salt) s FROM users WHERE cn = ?');
if(!$stmt)
{
    header('Location: error.html');
    $conn->close();
    exit;
}
$stmt->bind_param('s', $_SESSION['cn']);
$stmt->execute();
$result = $stmt->get_result();
if(!$result)
{
    header('Location: error.html');
    $stmt->close();
    $conn->close();
    exit;
}
if($result->num_rows > 0)
{
    $salt = hex2bin($result->fetch_assoc()['s']);
}
else
{
    $result->close();
    $stmt->close();
    $conn->close();
    header('Location: error.html');
    exit;
}
$result->close();
$stmt->close();
$stmt = $conn->prepare('UPDATE users SET mail = ?, displayname = ? WHERE cn = ? AND pw = UNHEX(?)');
if(!$stmt)
{
    header('Location: error.html');
    $conn->close();
    exit;
}
$curr = bin2hex(hash_pbkdf2('sha256', $_POST['old_pw'], $salt, 1000, 64, true));
$stmt->bind_param('ssss', $_POST['mail'], $_POST['name'], $_SESSION['cn'], $curr);
$stmt->execute();
if($stmt->affected_rows === 1)
{
    $_SESSION['mail'] = $_POST['mail'];
    $_SESSION['displayname'] = $_POST['name'];
    $stmt->close();
    $stmt = $conn->prepare('INSERT INTO log (cn, act) VALUES (?, 6)');
    if(!$stmt)
    {
        header('Location: error.html');
        $conn->close();
        exit;
    }
    $stmt->bind_param('s', $_SESSION['cn']);
    $stmt->execute();
    header('Location: .');
}
else
{
    header('Location: update_info.php');
}
$stmt->close();
$conn->close();
?>